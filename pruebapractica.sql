
DROP TABLE IF EXISTS `rol`;


CREATE TABLE `rol` (
  `id_rol` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='		';
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `rol` WRITE;

INSERT INTO `rol` VALUES (1,'ADMINISTRADOR'),(2,'AUDITOR'),(3,'AUXILIAR');

UNLOCK TABLES;



DROP TABLE IF EXISTS `usuario`

CREATE TABLE `usuario` (
  `id_usuario` int NOT NULL AUTO_INCREMENT,
  `id_rol` int NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `activo` int NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `fk_id_rol_idx` (`id_rol`),
  CONSTRAINT `fk_id_rol` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
