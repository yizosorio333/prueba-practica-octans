package clasesDao;

import Conexion.ConexionBD;
import modeloDTO.UsuarioDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class UsuarioDAO {

    private final String[] nombreColmns = {"idUsuario", "idRol", "Nombre", "Activo"};
    private Connection dbConnection;
    private PreparedStatement preparedStmt;

    public UsuarioDAO() {
        this.dbConnection = null;
        this.preparedStmt = null;
    }

    public int insertar(UsuarioDTO usuarioDTO) {
        int resultado = 0;
        try {
            dbConnection = ConexionBD.getConection();
            String insertSQL = "INSERT INTO usuario (id_rol, nombre, activo) VALUES (?,?,?)";
            preparedStmt = dbConnection.prepareStatement(insertSQL);
            preparedStmt.setInt(1, usuarioDTO.getIdRol());
            preparedStmt.setString(2, usuarioDTO.getNombre());
            preparedStmt.setInt(3, usuarioDTO.getActivo());
            resultado = preparedStmt.executeUpdate();
            dbConnection.close();
            preparedStmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "Error insertando registro");
        }
        return resultado;
    }

    public int actualizar(UsuarioDTO usuarioDTO) {
        int resultado = 0;
        try {
            dbConnection = ConexionBD.getConection();
            String sentencia = "UPDATE usuario SET nombre = ?,"
                    + "id_Rol = ?, activo = ?"
                    + " WHERE id_Usuario = ?";
            preparedStmt = dbConnection.prepareStatement(sentencia);
            preparedStmt.setString(1, usuarioDTO.getNombre());
            preparedStmt.setInt(2, usuarioDTO.getIdRol());
            preparedStmt.setInt(3, usuarioDTO.getActivo());
            preparedStmt.setInt(4, usuarioDTO.getIdUsuario());

            resultado = preparedStmt.executeUpdate();
            dbConnection.close();
            preparedStmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Error actualizando el registro");
        }
        return resultado;
    }

    public UsuarioDTO consultar(int cod) {
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        try {
            dbConnection = ConexionBD.getConection();
            String consulta = "SELECT * FROM usuario WHERE idUsuario = ?";
            preparedStmt = dbConnection.prepareStatement(consulta);
            preparedStmt.setInt(1, cod);
            ResultSet resultado = preparedStmt.executeQuery();
            if (resultado.next()) {
                usuarioDTO.setIdUsuario(resultado.getInt("idUsuario"));
                usuarioDTO.setIdRol(resultado.getInt("idRol"));
                usuarioDTO.setNombre(resultado.getString("nombre"));
                usuarioDTO.setActivo( resultado.getInt("activo"));

            } else {
                usuarioDTO = null;
            }
            dbConnection.close();
            preparedStmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Error buscando registro");
        }
        return usuarioDTO;
    }

    public int eliminar(int cod) {
        int resultado = 0;
        try {
            dbConnection = ConexionBD.getConection();
            String sentencia = "DELETE FROM usuario WHERE id_Usuario = ?";
            preparedStmt = dbConnection.prepareStatement(sentencia);
            preparedStmt.setInt(1, cod);
            resultado = preparedStmt.executeUpdate();
            dbConnection.close();
            preparedStmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "Error eliminando registro");
        }
        return resultado;
    }


    public DefaultTableModel consultarTodos(String valor) {
        DefaultTableModel modeloTb = new DefaultTableModel(null, nombreColmns);
        String[] registro = new String[4];
        try {
            dbConnection = ConexionBD.getConection();
            String consulta = "SELECT * FROM usuario WHERE nombre like '%"+valor+"%'";
            preparedStmt = dbConnection.prepareStatement(consulta);
            ResultSet resultado = preparedStmt.executeQuery();
            while (resultado.next()) {
                registro[0] = String.valueOf(resultado.getInt("id_Usuario"));
                int rol = resultado.getInt("id_Rol");
                if(rol==1){
                    registro[1] = "ADMINISTRADOR";
                }else if(rol==2){
                    registro[1] = "AUDITOR";                    
                }else {
                    registro[1] = "AUXILIAR";    
                }
                registro[2] = resultado.getString("nombre");
                registro[3] = resultado.getString("activo");

                modeloTb.addRow(registro);
            }
            dbConnection.close();
            preparedStmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Error buscando los registros");
        }
        return modeloTb;
    }

   

}
