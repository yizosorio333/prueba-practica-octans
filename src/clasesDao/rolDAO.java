package clasesDao;

import Conexion.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modeloDTO.RolDTO;

public class rolDAO {
    
     private final String[] nombreColmns = {"idRol", "nombre"};
    private Connection dbConnection;
    private PreparedStatement preparedStmt;

    public rolDAO() {
        this.dbConnection = null;
        this.preparedStmt = null;
    }

    public int insertar(RolDTO rolDTO) {
        int resultado = 0;
        try {
            dbConnection = ConexionBD.getConection();
            String insertSQL = "INSERT INTO rol VALUES (?,?)";
            preparedStmt = dbConnection.prepareStatement(insertSQL);
            preparedStmt.setInt(1, rolDTO.getIdRol());
            preparedStmt.setString(2, rolDTO.getNombre());
            resultado = preparedStmt.executeUpdate();
            dbConnection.close();
            preparedStmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "Error insertando registro");
        }
        return resultado;
    }

    public int actualizar(RolDTO rolDTO) {
        int resultado = 0;
        try {
            dbConnection = ConexionBD.getConection();
            String sentencia = "UPDATE rol SET nombre = ?,"
                    + " WHERE idRol = ?";
            preparedStmt = dbConnection.prepareStatement(sentencia);
            preparedStmt.setString(1, rolDTO.getNombre());
            preparedStmt.setInt(2, rolDTO.getIdRol());

            resultado = preparedStmt.executeUpdate();
            dbConnection.close();
            preparedStmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Error actualizando el registro");
        }
        return resultado;
    }

    public RolDTO consultar(int cod) {
        RolDTO rolDTO = new RolDTO();
        try {
            dbConnection = ConexionBD.getConection();
            String consulta = "SELECT * FROM rol WHERE idRol= ?";
            preparedStmt = dbConnection.prepareStatement(consulta);
            preparedStmt.setInt(1, cod);
            ResultSet resultado = preparedStmt.executeQuery();
            if (resultado.next()) {
                rolDTO.setIdRol(resultado.getInt("idRol"));
                rolDTO.setNombre(resultado.getString("nombre"));

            } else {
                rolDTO = null;
            }
            dbConnection.close();
            preparedStmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Error buscando registro");
        }
        return rolDTO;
    }

    public int eliminar(int cod) {
        int resultado = 0;
        try {
            dbConnection = ConexionBD.getConection();
            String sentencia = "DELETE FROM rol WHERE idRol= ?";
            preparedStmt = dbConnection.prepareStatement(sentencia);
            preparedStmt.setInt(1, cod);
            resultado = preparedStmt.executeUpdate();
            dbConnection.close();
            preparedStmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "Error eliminando registro");
        }
        return resultado;
    }

    public DefaultTableModel consultarTodos() {
        DefaultTableModel modeloTb = new DefaultTableModel(null, nombreColmns);
        String[] registro = new String[5];
        try {
            dbConnection = ConexionBD.getConection();
            String consulta = "SELECT * FROM rol";
            preparedStmt = dbConnection.prepareStatement(consulta);
            ResultSet resultado = preparedStmt.executeQuery();
            while (resultado.next()) {
                registro[0] = String.valueOf(resultado.getInt("idRol"));
                registro[1] = resultado.getString("nombre");

                modeloTb.addRow(registro);
            }
            dbConnection.close();
            preparedStmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Error buscando los registros");
        }
        return modeloTb;
    }

    
}
