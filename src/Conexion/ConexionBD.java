package Conexion;

import java.sql.*;
import javax.swing.JOptionPane;

public class ConexionBD {

    private static Connection conn;
    private static final String driver = "com.mysql.jdbc.Driver";
    public static final String JDBC_URL = "jdbc:mysql://localhost:3306/prueba?useSSL=false&useTimezone=true&serverTimezone=UTC&allowPublicKeyRetrieval=true";
    public static final String JDBC_USERNAME = "root";
    public static final String JDBC_PASSWORD = "admin";

    public static Connection getConection() {
        Connection conn = null;

        try {
            Class.forName(driver);
            conn = (Connection) DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD);
            System.out.println("Conexion Exitosa");
//            JOptionPane.showMessageDialog(null, "Conexion Exitosa");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Hubo un problema al intentar conectar con la base de datos.");
        }
        return conn;
    }
    
     public static void setConexion() {
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
